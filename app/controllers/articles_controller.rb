class ArticlesController < ApplicationController
  before_action :authenticate_user!, only: %i[new create edit update destroy] 

  def index
    @articles = Article.paginate(:page=>params[:page],per_page:2)
  end
  def show
    @article = Article.find(params[:id])
  end
  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    @article.author = current_user
    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end
  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit, status: :unprocessable_entity
    end
  end
  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path
  end
  private
  def article_params
    params.require(:article).permit(:title, :body, :status)
  end
end
